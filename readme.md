# Data Structures and Algorithm problems/solutions in JavaScript

## Programming Problems

1. 2D Array DS
    - [Problem](https://www.hackerrank.com/challenges/2d-array/problem)
    - [Solution](./2d-array-ds/solution.js)

2. Arrays DS
    - [Problem](https://www.hackerrank.com/challenges/arrays-ds/problem)
    - [Solution](./arrays-ds/solution.js)

3. Array Left Rotation
    - [Problem](https://www.hackerrank.com/challenges/array-left-rotation/problem)
    - [Solution](./array-left-rotation/solution.js)
    
4. Sparse Arrays
    - [Problem](https://www.hackerrank.com/challenges/sparse-arrays/problem)
    - [Solution](./sparse-arrays/solution.js)

5. Dynamic Array
    - [Problem](https://www.hackerrank.com/challenges/dynamic-array/problem)
    - [Solution](./dynamic-array/solution.js)

6. Array Manipulation
    - [Problem](https://www.hackerrank.com/challenges/crush/problem)
    - [Solution](./array-manipulation/solution.js)

