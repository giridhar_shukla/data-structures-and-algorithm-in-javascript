"use strict";

const fs = require("fs");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", inputStdin => {
  inputString += inputStdin;
});

process.stdin.on("end", _ => {
  inputString = inputString
    .replace(/\s*$/, "")
    .split("\n")
    .map(str => str.replace(/\s*$/, ""));

  main();
});

function readLine() {
  return inputString[currentLine++];
}

function applyLeftRotation(d, inputArray) {
  const rotatingStuff = inputArray.splice(d);
  const output = rotatingStuff.concat(inputArray);
  return output;
}

function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);
  const nd = readLine().split(" ");

  const n = parseInt(nd[0], 10);

  const d = parseInt(nd[1], 10);

  const a = readLine()
    .split(" ")
    .map(aTemp => parseInt(aTemp, 10));

  const res = applyLeftRotation(d, a);

  ws.write(res.join(" ") + "\n");
  ws.end();
}
