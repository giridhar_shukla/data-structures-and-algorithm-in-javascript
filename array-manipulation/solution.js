/** THIS SOLUTION IS NOT QUITE EFFICIENT. IT PASSED ONLY 8 OUT OF 15 TESTCASES */
"use strict";

const fs = require("fs");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", inputStdin => {
  inputString += inputStdin;
});

process.stdin.on("end", _ => {
  inputString = inputString
    .replace(/\s*$/, "")
    .split("\n")
    .map(str => str.replace(/\s*$/, ""));

  main();
});

function readLine() {
  return inputString[currentLine++];
}

// Complete the arrayManipulation function below.
function arrayManipulation(n, queries) {
  let maxNumber = 0;
  let manipulatedNumbers = {};
  queries.forEach((query, key) => {
    console.log(key);
    let startIdx = query[0];
    let endIdx = query[1];
    let summand = query[2];
    for (let i = startIdx; i <= endIdx; i++) {
      if (!manipulatedNumbers[i]) {
        manipulatedNumbers[i] = 0;
      }
      manipulatedNumbers[i] += summand;

      //after changing every items, check for maximum entity
      if (manipulatedNumbers[i] > maxNumber) {
        maxNumber = manipulatedNumbers[i];
      }
    }
  });
  return maxNumber;
}

function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const nm = readLine().split(" ");

  const n = parseInt(nm[0], 10);

  const m = parseInt(nm[1], 10);

  let queries = new Array(); //Array(m);

  for (let i = 0; i < m; i++) {
    let readData = readLine();
    if (readData == undefined) {
      break;
    }
    queries[i] = readData
      .split(" ")
      .map(queriesTemp => parseInt(queriesTemp, 10));
  }
  console.log(queries);

  let result = arrayManipulation(n, queries);

  ws.write(result + "\n");

  ws.end();
}
